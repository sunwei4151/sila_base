import HowToImplementSila from "../documentation/docs/implementation/How-to-implement-SiLA.md";
import Troubleshooting from "../documentation/docs/implementation/Troubleshooting.md";
import WhatIsSila from "../documentation/docs/introduction/What-is-SiLA.md";

export default [
    {
        source: HowToImplementSila,
        path: "/docs/implementation/how-to-implement-sila",
    },
    {
        source: Troubleshooting,
        path: "/docs/implementation/troubleshooting",
    },
    {
        source: WhatIsSila,
        path: "/docs/introduction/what-is-sila",
    },
];
